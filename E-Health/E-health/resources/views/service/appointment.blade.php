 @extends('layouts.app')
@section('content')
 <section class="appointment">

        <div class="container">
            <h2 class="text-center">Make An Appointment</h2>
            <hr>
            <div class="app">
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-xs-12 m-t-40">
                      <form>
                        <div class="form-group">
                          <label>First Name</label>
                            <br>
                            <input type="text" name="name" placeholder="First Name" required class="form-control input-name">
                          </div>

                          <div class="form-group">
                          <label>Last Name</label>
                            <br>
                            <input type="text" name="name" placeholder="Last Name" required class="form-control input-name">
                          </div>

                          <div class="form-group">
                          <label>Phone Number</label>
                            <br>
                            <input type="text" name="name" placeholder="Phone Number" required class="form-control input-name">
                          </div>

                          <div class="form-group">
                          <label>Email</label>
                            <br>
                            <input type="email" name="name" placeholder="Email" required class="form-control input-name">
                          </div>

                          <div class="form-group">
                              <div class="row">
                                    <div class="col-md-6">
                                        <label>Date</label>
                                        <br>
                                        <div class="input-group date">
                                            <input type="text" class="form-control" placeholder="MM/DD/YYYY" name="dates" required="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="clearfix visible-xs-block"></div>
                                    <div class="col-md-6">
                                        <label>Time</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control" placeholder="HH:MM" name="hours" required="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                          </div>

                          <div class="text-center m-t-20">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>

              

                </div>

            </div>

        </div>

    </section>
@endsection