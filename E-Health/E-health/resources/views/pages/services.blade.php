@extends('layouts.app')
@section('content')
    
<div id="section-what-we-do" class="section section-background inverse">
                        <div class="container text-center">
                            <div class="section-heading">
                                <div class="title">Our service</div>
                                <div class="line"></div>
                            </div>
                            <div class="section-content">
                                <div class="list-departments">
                                    <div class="row mbxxl">
                                        <div class="col-md-3">
                                            <a href="/doctor"><img src="/images/medical-icon/doctor.png" alt="" class="img-responsive" />
                                             <p>Doctors</p>
                                            </a>
                                        </div>
                                         <div class="col-md-3">
                                            <a href="/ambulance"><img src="/images/medical-icon/ambulance.png" alt="" class="img-responsive" />
                                            <p>Ambulance</p>
                                            </a>
                                        </div> 
                                        <div class="col-md-3">
                                            <a href="/blood_donor"><img src="/images/medical-icon/blood_donor.png" alt="" class="img-responsive" />
                                            <p>Blood Bank</p>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="/appointment"><img src="/images/medical-icon/appointment.png" alt="" class="img-responsive" />
                                            <p>Doctor's Appointment</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection