@extends('layouts.app')
@section('content')
<h1>Register</h1>
    {!! Form::open(['action'=>'RegisterController@validator','method' => 'validator' ]) !!}
        
        <div class="form-group">
            {{Form::label('name','Full Name')}}
            {{Form::text('name','',['class'=>'form-control','placeholder'=>'Full name'])}}
        </div>
        
        <div class="form-group">
            {{Form::label('email','email')}}
            {{Form::text('email','',['class'=>'form-control','placeholder'=>'email@team1.com'])}}
        </div>
        
          <div class="form-group">
            {{Form::label('password','Password')}}
            {{Form::text('password','',['class'=>'form-control','placeholder'=>'Password'])}}
        </div>

        {{Form::submit('Register',['class'=>'btn btn-primary'])}}

    {!! Form::close() !!}
@endsection

