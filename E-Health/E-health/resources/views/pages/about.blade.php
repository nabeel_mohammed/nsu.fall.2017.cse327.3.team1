@extends('layouts.app')
@section('content')
     
    <section class="about">
        <div class="container">
             <div class="about_us">
                <h1 class="text-center underline">About Us</h1>
                <hr>
                <div class="row m-t-55">
                     <div class="col-md-5 col-md-push-7 m-t-40">               
                </div>
                 <div class="col-md-7 col-md-pull-5 m-t-20">
                     <h3 class="head_left_underline">Consult The Best Specialists At any hospital</h3>
                     <hr>
                     <div class="m-t-20">
                        <p>
                            service providing medium for doctors and patients.
                        </p>
                        <p>
                            Consult doctors from this page.
                        </p>
                            <a href="/contact" class="btn btn-primary m-t-10">
                            Contact Us
                            </a>
                     </div>
                 </div>

                </div>
            </div>
        </div>
    </section>
    
@endsection


