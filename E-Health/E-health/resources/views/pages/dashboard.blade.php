@extends('layouts.app')
@section('content')
    
 <div class="jumbotron text-center">
    <h1>{{$title}}</h1>
    <p> this my test index page </p>

    <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/n1.jpg" alt="1">
                <div class="carousel-caption hidden-xs">
                    <h2 class="h1">Services</h2>
                    <p class="lead">paragraph </p>
                </div>
            </div>

            <div class="item">
                <img src="img/n2.jpg" alt="2">
                <div class="carousel-caption hidden-xs">
                    <h2 class="h1">Services</h2>
                    <p class="lead">paragraph </div>
            </div>

            <div class="item">
                <img src="img/n3.jpg" alt="3">
                <div class="carousel-caption hidden-xs">
                    <h2 class="h1">Services</h2>
                    <p class="lead">paragraph </div>
            </div>
    </div>
@endsection