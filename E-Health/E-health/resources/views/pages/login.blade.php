@extends('layouts.app')
@section('content')
<h1>Loging</h1>


<section class="register">

        
                <div class="login">
                     <div class="login__check"></div>
                         <div class="login__form">
                            <div class="login__row">
                                <label>User name:</label>
                                <input type="text" class="login__input name" placeholder=" email@team1.com "/>
                            </div>
        
                            <div class="login__row">
                                <label>Password    :</label>
                                <input type="password" class="login__input pass" placeholder="Password"/>
                            </div>
                            <button type="button" class="btn btn-primary login__submit">Sign in</button>
                            <p class="register">Don't have an account? &nbsp;<a href="register">Register</a></p>
                        </div>
                </div>

        
            <script src="{{ URL::asset('http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js') }}"></script>

            <script src="{{ URL::asset('js/sign_in.js') }}"></script>
           
</section>

@endsection