<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','PagesController@index');
Route::get('/about','PagesController@about');
Route::get('/services','PagesController@services');
Route::get('/login','PagesController@login');
Route::get('/register','PagesController@register');
Route::get('/contact','PagesController@contact');
Route::get('/doctor','PagesController@doctor');


Route::get('/appointment','ServiceController@appointment');
Route::get('/ambulance','ServiceController@ambulance');
Route::get('/bloodbank','ServiceController@bloodbank');


Route::get('/dashboard', 'HomeController@index');

Route::resource('posts','PostController');


Auth::routes();



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
